package featureflags

import (
	gocache "github.com/patrickmn/go-cache"
)

/*
Saves a featureflag in cache only, for testing
 */
func SetMockFeatureFlag(name string, service string, value interface{}) {
	cache.Set(featureKey(name, service), value, gocache.DefaultExpiration)
}
