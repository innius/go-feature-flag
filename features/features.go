package features

const (
	// XRAY enable xray middleware for service http requests
	XRAY = "xray"

	// use route53 for service discovery
	Route53ServiceDiscovery = "route53_service_discovery"

	// use consul client to discovery legacy services
	ConsulClientServiceDiscovery = "consul_client_service_discovery"

	// enable the new collaboration login flow
	NewLoginFlow = "new_login_flow"

	// Body dump middleware captures the request and response payload
	HttpBodyDump = "http_body_dump"

	// replace collaboration service with the new machine service
	NewMachineService = "new_machine_service"
)
