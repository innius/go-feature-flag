# go-feature-flag

Simple feature flag package for innius services 

Feature flags are read from the environment or the aws Parameter Store (ssm)

Feature flags read from SSM are cached for 1 minute

## usage 

```
import bitbucket.org/to-increase/features

```

to get a service feature flag: 
```
features.FeatureEnabled("feature_x", "my-service")
```

to get a stack feature flag: 
```
features.FeatureEnabled("feature_x", "my-service")
```

## IAM Permissions
Ensure the EC2 of the service has read permissions on SSM 




