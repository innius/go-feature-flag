# go-feature-flags v2

Simple feature flag package for innius services 


## usage 

```
import bitbucket.org/to-increase/featureflags/v2
```
configure the feature flags for a service: 
```go
featureflags.Configure(store, stackName, serviceName)
```

to get a service feature flag: 
```
featureflags.Enabled(feature)
```

to get a stack feature flag: 
```
featureflags.Enabled(feature, stackName)
```
