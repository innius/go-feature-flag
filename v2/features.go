package featureflags

import "strings"

type Config struct {
	Prefix string
	Store  Store
}

type Store interface {
	Enabled(string) bool
}

var cfg = &Config{
	Store: NewMemoryStore(),
}

// Configures the featureflag;
func Configure(store Store, prefix ...string) {
	cfg.Prefix = formatPrefix(prefix...)

	if store != nil {
		cfg.Store = store
	}
}

func formatPrefix(prefix ...string) string {
	if len(prefix) > 0 {
		return "/" + strings.Join(prefix, "/") + "/"
	}
	return ""
}

// Enabled checks whether or not the specified feature is enabled.
// Optional prefix param allows override of configured prefix
func Enabled(feature string, prefix ...string) bool {
	pf := cfg.Prefix
	if len(prefix) > 0 {
		pf = formatPrefix(prefix...)
	}

	return cfg.Store.Enabled(pf + feature)
}
