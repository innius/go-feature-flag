package featureflags_test

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/innius/go-feature-flag/v2"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"github.com/stretchr/testify/assert"
)

type ssmMock struct {
	ssmiface.SSMAPI
	invocations int
}

func (m *ssmMock) GetParameter(*ssm.GetParameterInput) (*ssm.GetParameterOutput, error) {
	m.invocations++
	return &ssm.GetParameterOutput{
		Parameter: &ssm.Parameter{
			Value: aws.String("true"),
		},
	}, nil
}

func TestFeatureFlags(t *testing.T) {
	t.Run("Feature Flags", func(t *testing.T) {
		serviceName := "foo-service"
		stackName := "chair"
		feature := "foo"

		t.Run("without a prefix", func(t *testing.T) {
			store := featureflags.NewMemoryStore()
			featureflags.Configure(store)
			store.Enable(feature)
		})
		t.Run("read an enabled feature", func(t *testing.T) {
			assert.True(t, featureflags.Enabled(feature))
		})

		store := featureflags.NewMemoryStore()
		featureflags.Configure(store, stackName, serviceName)
		store.Enable(fmt.Sprintf("/%s/%s/%s", stackName, serviceName, feature))

		t.Run("read an enabled service feature flag", func(t *testing.T) {
			assert.True(t, featureflags.Enabled(feature))
		})
		t.Run("read a disabled service feature flag", func(t *testing.T) {
			assert.False(t, featureflags.Enabled("feature2"))
		})
		t.Run("use a different prefix", func(t *testing.T) {
			feature := "global_feature"
			store.Enable(fmt.Sprintf("/%s/%s", stackName, feature))
			assert.True(t, featureflags.Enabled(feature, stackName))
		})
	})
	t.Run("with an SSM Store", func(t *testing.T) {
		serviceName := "foo-service"
		stackName := "chair"
		feature := "foo"
		m := &ssmMock{}
		store := featureflags.NewSSMStore(m, 1*time.Second)

		featureflags.Configure(store, stackName, serviceName)
		
		t.Run("read the feature flag", func(t *testing.T) {
			assert.True(t, featureflags.Enabled(feature))
		})
		t.Run("read the feature flag again", func(t *testing.T) {
			assert.True(t, featureflags.Enabled(feature))
		})
		t.Run("it should return the cached value", func(t *testing.T) {
			assert.Equal(t, 1, m.invocations)
		})
	})
}
