package featureflags

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"github.com/patrickmn/go-cache"

	"log"
)

func NewSSMStore(ssmapi ssmiface.SSMAPI, expiration time.Duration) Store {
	return &ssmStore{
		ssmapi: ssmapi,
		cache:  cache.New(expiration, 10*time.Minute),
	}
}

type ssmStore struct {
	cache  *cache.Cache
	ssmapi ssmiface.SSMAPI
}

func (s *ssmStore) Enabled(feature string) bool {
	if v, ok := s.cache.Get(feature); ok {
		switch t := v.(type) {
		case bool:
			return t
		}
	}

	res, err := s.ssmapi.GetParameter(&ssm.GetParameterInput{Name: aws.String(feature)})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case ssm.ErrCodeParameterNotFound:
				s.cache.Set(feature, false, cache.DefaultExpiration)
				return false
			}
		}
		log.Print(fmt.Sprintf("could not read featureflag %s; %s", feature, err.Error()))
		return false
	}

	enabled := parseBool(aws.StringValue(res.Parameter.Value))
	s.cache.Set(feature, enabled, cache.DefaultExpiration)
	return enabled
}

func parseBool(v string) bool {
	if s, err := strconv.ParseBool(v); err == nil {
		return s
	}
	return false
}
