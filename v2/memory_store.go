package featureflags

func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		store: map[string]bool{},
	}
}

type MemoryStore struct {
	store map[string]bool
}

func (s *MemoryStore) Enabled(feature string) bool {
	return s.store[feature]
}

func (s *MemoryStore) Enable(feature string) {
	s.store[feature] = true
}

func (s *MemoryStore) Disable(feature string) {
	s.store[feature] = false
}
