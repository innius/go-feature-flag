module bitbucket.org/innius/go-feature-flag/v2

go 1.20

require (
	github.com/aws/aws-sdk-go v1.15.84
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/smartystreets/goconvey v0.0.0-20190222223459-a17d461953aa
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20160202185014-0b12d6b521d8 // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
