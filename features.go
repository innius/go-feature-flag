package featureflags

import (
	"fmt"
	"log"

	"os"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"

	gocache "github.com/patrickmn/go-cache"
)

var stackName *string

// Create a cache with a default expiration time of 1 minute, and which
// purges expired items every 10 minutes
var cache = gocache.New(1*time.Minute, 10*time.Minute)

// FeatureEnabled checks whether or not the specified feature is enabled
// the service name is optional; if omited the feature is expected to be
// a global stack feature
func FeatureEnabled(name string, service ...string) bool {
	return featureEnabled(featureKey(name, service...))
}

// ServiceName returns the name of the current service based on environment name
// this service is always set for innius ECS task services and might be set for others
func ServiceName() string {
	return os.Getenv("SERVICE_NAME")
}

// format feature / service to a ssm key path
func featureKey(name string, service ...string) string {
	flag := []string{"", GetEnvironmentName()}
	if len(service) == 1 && service[0] != "" {
		flag = append(flag, service[0])
	}
	flag = append(flag, name)

	return strings.Join(flag, "/")
}

func featureEnabled(name string) bool {
	// check the cache
	if v, ok := cache.Get(name); ok {
		switch t := v.(type) {
		case bool:
			return t
		}
	}
	enabled := getParameterFromSSM(name)
	cache.Set(name, enabled, gocache.DefaultExpiration)
	return enabled
}

var client ssmiface.SSMAPI = ssm.New(session.New())
var ec2client ec2iface.EC2API = ec2.New(session.New())

var getParameterFromSSM = func(name string) bool {
	req := &ssm.GetParameterInput{Name: aws.String(name)}

	v, err := client.GetParameter(req)
	if err != nil {
		return false
	}
	return parseBool(aws.StringValue(v.Parameter.Value))
}

func parseBool(v string) bool {
	if s, err := strconv.ParseBool(v); err == nil {
		return s
	}
	return false
}

func instanceID() *string {
	client := ec2metadata.New(session.New())
	if !client.Available() {
		return nil
	}
	id, err := client.GetInstanceIdentityDocument()
	if err != nil {
		return nil
	}

	return aws.String(id.InstanceID)
}

//DEPRICATED use EnvironmentName
func GetEnvironmentName() string {
	return EnvironmentName()
}

// GetEnvironmentName returns the name of the innius stack in which a service is deployed
func EnvironmentName() string {
	// get the environment name from environment vars (with legacy here)
	if s, ok := os.LookupEnv("EnvironmentStackName"); ok {
		return s
	}
	if s, ok := os.LookupEnv("ENVIRONMENT_NAME"); ok {
		return s
	}
	if s, ok := os.LookupEnv("EnvironmentName"); ok {
		return s
	}

	if stackName == nil {
		id := instanceID()
		if id != nil {
			stackName = getStackNameFromEnvironment(id)
		}
	}
	return aws.StringValue(stackName)
}

func getStackNameFromEnvironment(id *string) *string {
	out, err := ec2client.DescribeTags(&ec2.DescribeTagsInput{
		Filters: []*ec2.Filter{
			{
				Name: aws.String("resource-type"),
				Values: []*string{
					aws.String("instance"),
				},
			},
			{
				Name: aws.String("resource-id"),
				Values: []*string{
					id,
				},
			},
			{
				Name: aws.String("key"),
				Values: []*string{
					aws.String("EnvironmentName"),
				},
			},
		},
	})
	if err != nil {
		log.Print(fmt.Sprintf("could not get the stackname from environment; %+v", err))
		return nil
	}

	if len(out.Tags) > 0 {
		return out.Tags[0].Value
	}
	return nil
}
