module bitbucket.org/innius/go-feature-flag

require (
	github.com/aws/aws-sdk-go v1.15.81
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c
	github.com/stretchr/testify v1.2.2
)
