package featureflags

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"github.com/stretchr/testify/assert"
)

func TestServiceFeatureFlagKey(t *testing.T) {
	stackName = aws.String("foo")

	assert.Equal(t, "/foo/a-service/bar", featureKey("bar", "a-service"))
}

func TestGlobalFeatureFlagKey(t *testing.T) {
	stackName = aws.String("foo")

	assert.Equal(t, "/foo/bar", featureKey("bar", ""))
	assert.Equal(t, "/foo/bar", featureKey("bar"))
}

func TestGetEnvironmentName(t *testing.T) {
	ec2client = &mockEC2Client{}
	assert.Equal(t, aws.String("bar"), getStackNameFromEnvironment(nil))
}

type mockEC2Client struct {
	ec2iface.EC2API
}

func (m *mockEC2Client) DescribeTags(input *ec2.DescribeTagsInput) (*ec2.DescribeTagsOutput, error) {
	return &ec2.DescribeTagsOutput{Tags: []*ec2.TagDescription{
		{Key: aws.String("foo"), Value: aws.String("bar")},
	}}, nil
}

type mockSSMClient struct {
	ssmiface.SSMAPI
	count int
}

func (m *mockSSMClient) GetParameter(in *ssm.GetParameterInput) (*ssm.GetParameterOutput, error) {
	value := ""
	if aws.StringValue(in.Name) == "/foo/my-service/my-foo" {
		value = "true"
	}
	m.count++
	return &ssm.GetParameterOutput{
		Parameter: &ssm.Parameter{Name: in.Name, Value: aws.String(value)},
	}, nil
}

func TestFeatureEnabled(t *testing.T) {
	mock := &mockSSMClient{}
	client = mock
	ec2client = &mockEC2Client{}

	assert.True(t, FeatureEnabled("my-foo", "my-service"))
	assert.True(t, FeatureEnabled("my-foo", "my-service"))
	assert.Equal(t, 1, mock.count) // verify the cache
}
